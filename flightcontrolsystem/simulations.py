""" Simulators for each sensor """
import random
from flightsimexception import FlightSimException
#each instance of the simulation needs to be global to be
#accessed by program.  If the classes were instantiated elsewhere
#this would not be needed.  This simulation emulates hardware
#so this becomes an association/aggrigation
global tach1_
global tach2_
global compass_
global engTemp1_
global engTemp2_
global fuelL_
global fuelC_
global fuelR_
global airspeed_

global UPDATE_PERIOD
UPDATE_PERIOD = 200


class Simulator:
    """Base Class for all simulators"""

    def __init__(self,devID:str,initVal:float) -> None:
        """ all simulations will require a devID, the GUI ID of the 
        input 'device' as a string;
        and an initial value as a default output"""
        self.devID = devID
        self.value = initVal # default value

    def update(self,values:dict) -> int:
        """values is the dictionary of values of which the devID is the key
        Base Class Simulation just returns initVal from constructor
        Need to override to get simulated data"""
        self.value = values
        return self.value

class Compass(Simulator):
    """simulates a compass and related readings"""
    _value = 0.0
    comp_points = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW']

    def __init__(self, devID:str, initVal:float):
        """subclass compass of the simulator """
        super().__init__(devID, initVal)
        #initVal is the value passed into the compass (what direction you are going in degrees)
        Compass._value = initVal #22 
        

    def update(self, values:dict = None) -> float:
        delta = random.random() - 0.5 + 0.005 #between [0.505 and -0.495]
        Compass._value = (delta + Compass._value) % 360 #update compass value
        return float(Compass._value)

    @classmethod
    def setCompass(cls, new_reading:int): #not related to instance but to compass class
        Compass._value = new_reading % 360
                
class DelaySim(Simulator):
    """creates a filter which causes a defined lag/delay in the response.
    """
    def __init__(self, devID: str, initVal: float, outLow: float,
        outHigh:float, delay: int) -> None:
        super().__init__(devID, initVal)
        """devID is the device used for input with range 0 - 100
            initVal is the initial value of the output
            outLow and outHigh define the range of the output device
            delay is the delay in ms"""
            
        self._delay = delay
        numback = self.delay//UPDATE_PERIOD # number of backvalues
        self.backvalues = [initVal] * numback
        self.slope = (outHigh - outLow)/100.0
        self.offset = outLow / self.slope
        
        self.state = 'safe'

    def update(self,values:dict) -> float:
        """
        uses the newval to the filter and returns the filtered value"""
        val = (values[self.devID] - self.offset) * self.slope
        self.backvalues.pop(0)
        self.backvalues.append(val)
        self.value = (sum(self.backvalues)/len(self.backvalues))

        return self.value

    #Use a property called delay to change the delay of an existing (running) instance of DelaySim.  
    #Hint:  the value is in milliseconds;  the number of backvalues will need to be adjusted.

    @property
    def delay(self) -> float:
        return self._delay
    
    @delay.setter
    def delay_set(self, delay:float) -> None:
        self._delay = delay



class Fuel(Simulator):
    """calculate and display fuel usage"""
    BURN_RATE = .002
    def __init__(self, devID: str, initVal: float, optional_throttle:str = None) -> None:
        super().__init__(devID, initVal)
        self.__total = initVal/100
        self.optional_throttle = optional_throttle
    
        self.devID = devID


    def __str__(self) -> str:
        pass

    def update(self, values: dict) -> float: #values is the dictionary with keyword, and a number pointing to input value
        """update fuel amount"""
        #utilize the values read from either one or both throttle devices
        #if landing geat is UP burn rate is .001 and DOWN burn rate is .002
        if values[self.optional_throttle] == None:
            self.__total = self.__total - (Fuel.BURN_RATE * values[self.devID]) #Previous total – (fuel burn rate * average throttle value for each engine)
        else:
            self.__total = self.__total - (Fuel.BURN_RATE * (values[self.devID] + values[self.optional_throttle])/2)
        return self.__total
    
    # called when the rate is changed! (LANDING GEAR UP/DOWN)
    @classmethod
    def set_burn_rate(cls, newRate:float):
        """change fuel rate when landing gear is up or down. this can be done by simply setting burn rate. 
        the rest will carry on automatically.
        call on event, edits fuel burn rate 
        simulations.Fuel.set_burn_rate(.002)"""
        Fuel.BURN_RATE = newRate
    
    # self._total was defined in the constructor as a 'private' variable
    @property
    def total(self) -> float: # returns total amount of fuel in pounds
        return (self.__total * 100)

    @total.setter
    def total(self,total:int): # the new total amount of fuel, not just the added fuel
        if total > 10000:
            self.__total = 10000/100
            raise FlightSimException("Fuel OverfFlow")
        else:
            self.__total = total/100 #apply scaling
        
DELAY_TACH = 1500 #ms
INITIAL_TACH = 600 #RPM
LOW_RANGE_TACH = 0
HIGH_RANGE_TACH = 3850.0

tach1_ = DelaySim('-THR1-', INITIAL_TACH, LOW_RANGE_TACH, HIGH_RANGE_TACH, DELAY_TACH)
tach2_ = DelaySim('-THR2-', INITIAL_TACH, LOW_RANGE_TACH, HIGH_RANGE_TACH, DELAY_TACH)

DELAY_TMP = 10000 #ms  
INITIAL_TMP = 0.15 #volts  
LOW_RANGE_TMP = 0.0 #volts = - 30 degrees F  
HIGH_RANGE_TMP = 1.3 #volts = 250 degrees F

engTemp1_ = DelaySim('-THR1-', INITIAL_TMP, LOW_RANGE_TMP, HIGH_RANGE_TMP, DELAY_TMP)
engTemp2_ = DelaySim('-THR2-', INITIAL_TMP, LOW_RANGE_TMP, HIGH_RANGE_TMP, DELAY_TMP)

INIT_CENTER = 9000
INIT_WING = 8000

fuelC_ = Fuel('-THR1-', INIT_CENTER, '-THR2-')
fuelL_ = Fuel('-THR1-', INIT_WING, '-THR2-')
fuelR_ = Fuel('-THR1-', INIT_WING, '-THR2-')

INIT_VALUE_COMPASS = 22
#the compass has an initial value of 22
compass_ = Compass('-COMPASS-', INIT_VALUE_COMPASS)

