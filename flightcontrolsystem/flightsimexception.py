"""The primary exception for the flight sim applicaton"""
#inherits all exceptions
class FlightSimException(Exception):
    pass

class SensorException(Exception):
    pass