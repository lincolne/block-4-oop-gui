""" The Sensors class is often called a wrapper which
provides a common software intface to different types
of hardware sensors.  Each type of device will currently
connect to a simulator, but in the near future they will
individually connect to hardware
 """
 
from simulations import Simulator
from flightsimexception import FlightSimException
class Sensor:
    """the sensor class senses readings from sim"""
    def __init__(self, sim:Simulator) -> None:
        """call this function when you want to create a new sensor"""
        self.sim = sim
    def read_sensor(self, values:dict) -> float:
        """reads the sensor data from simulator"""
        if self.sim is not None:
            return self.sim.update(values)

class Tach_sensor(Sensor):
    def __init__(self, sim: Simulator) -> None:
        super().__init__(sim)
        self.TACH_SCALE_FACTOR = 0.025 
        self.state = 'safe'

    def read_sensor(self, values: dict) -> float:
        """reads sensor information, updates it by the scale factor of 0.025, then compares that value so that if it goes beyond the boundary,
        it will display errors."""
        value = self.sim.update(values) #Call the update method of the simulator passing in the parameter from the method call
        value = value * self.TACH_SCALE_FACTOR
        match self.state: #all of this code works but only for engine temp not RPM??
            case 'safe':
                if value >= 70 and value < 90:
                    self.state = 'caution'
                    raise FlightSimException(f'RPM: {int(value)} - Engine RPM is YELLOW')
            case 'caution':
                if value >= 90:
                    self.state = 'warn'
                    raise FlightSimException(f'RPM: {int(value)} - Engine RPM is REDLINE')
                if value < 70:
                    self.state = 'safe'
                    raise FlightSimException(f'RPM: {int(value)} - Engine RPM returned to normal.')
            case 'warn':
                if value >= 70 and value < 90:
                    self.state = 'caution'
        return value

class Tempr_sensor(Sensor):
    def __init__(self, sim: Simulator) -> None:
        super().__init__(sim)
        self.TEMPR_SCALE_FACTOR = 75.0/1.3
        self.OFFSET = -20.0

    def read_sensor(self, values:dict) -> float:
        value = self.sim.update(values) #Call the update method of the simulator passing in the parameter from the method call  
        value = value * self.TEMPR_SCALE_FACTOR - self.OFFSET
        return value

class Fuel_sensor(Sensor):
    """sense fuel leve3l, make no changes"""
    def __init__(self, sim: Simulator) -> None:
        super().__init__(sim)
    
    def read_sensor(self, values: dict) -> float:
        value = self.sim.update(values)
        return value
    
    @staticmethod
    def to_gallons(pounds:float) -> float:
        """converts pounds to gallons:
        Fuel_sensor.to_gallons(pounds)"""
        gallons = pounds * 6.5
        return gallons

class Comp_sensor(Sensor):
    '''senses compass information, make no changes'''
    def __init__(self, sim: Simulator) -> None:
        super().__init__(sim)
    def read_sensor(self, values: dict) -> float:
        value = self.sim.update(values)
        #print(value) this is the dictionary constantly being updated
        return value
        


# print(Fuel_sensor.to_gallons(10)) #prints 65 