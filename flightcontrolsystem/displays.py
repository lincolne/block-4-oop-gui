""" Displays for the Cockpit Simulation System """
from typing import Callable     #used for a parameter hint
from flightsimexception import FlightSimException

class Display:
    '''displays simulation information from the sensor'''
    def __init__(self, display:Callable):
        """creates display"""
        self.display = display
    def update(self, screen:float):
        """updates the display with new information"""
        self.display(screen)
    
    @staticmethod
    def validate_range(value:int) -> bool:
        return 0 <= value <= 100

class Bar(Display):
    """Bar display class: inherits display but also passes 3 ints: caution, warn, limit"""
    def __init__(self, display: Callable, caution:int, warn:int, limit:int):
        super().__init__(display)
        #save as instance variables using self
        self.caution = caution
        self.warn = warn
        self.limit = limit
    
    def update(self, value: float):
        self.display(value)

class Fuel_level(Display):
    """Display fuel level, make no changes"""
    def __init__(self, display: Callable):
        super().__init__(display)
        #save as instance variables using self
        self.state = 'safe'
        
    
    def update(self, value: float):

        super().update(value)
        match self.state:
            case 'safe':
                #check
                if not Display.validate_range(value):
                    self.state = 'empty'
                    raise FlightSimException('Fuel tank is empty!')
            #this works but i copied what was on board but it doesnt work
            case 'empty':
                if Display.validate_range(value):
                    self.state = 'safe'
                    raise FlightSimException("Fuel Low")
            case _:
                raise FlightSimException('Error 223')

    # @property
    # def total(self) -> float:
    #     return self.__total*100
    
    # @total.setter
    # def total(self, total:int):
    #     if total > 10000:
    #         self.__total = 10000/100
    #         raise FlightSimException("Fuel Overflow!")
    #     else:
    #         self.__total = total/100
            
class CompassDisp(Display):
    '''Compass display class to take in data from sim and output to GUI'''
    #comp_points = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW']

    def __init__(self, numericDisp: Callable, cardinalDisp: Callable):
        self.numericDisp = numericDisp
        self.cardinalDisp = cardinalDisp
        self.direction = ''
        # self.cardinalDisp = cardinalDisp

    def update(self, degrees: float):
        '''update and display the directions of the compass onto the gui
        numeric display is the degrees
        cardinal display is the direction'''
        #there could have been a more efficient way of doing this, cycling through the list comp_points
        if degrees <= 22.5 or degrees > 337.5:
            self.direction = 'N'
        elif degrees > 22.5 and degrees <= 67.5:
            self.direction = 'NE'
        elif degrees > 67.5 and degrees <= 112.5:
            self.direction = 'E'
        elif degrees > 112.5 and degrees <= 157.5:
            self.direction = 'SE'
        elif degrees > 157.5 and degrees <= 202.5:
            self.direction = 'S'
        elif degrees > 202.5 and degrees <= 247.5:
            self.direction = 'SW'
        elif degrees > 247.5 and degrees <= 292.5:
            self.direction = 'W'
        elif degrees > 292.5 and degrees <= 337.5:
            self.direction = 'NW'

        #display degrees as an integer
        # print(f"Degrees: {degrees}")
        self.numericDisp(round(degrees))
        #display direction
        self.cardinalDisp(self.direction)
